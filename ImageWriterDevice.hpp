//
// Created by jaanus on 16.10.16.
//

#pragma once

#include "SensorDevice.hpp"
#include <stdint.h>
#include <string>
#include <opencv2/opencv.hpp>
#include <experimental/filesystem>

using namespace std;

class ImageWriterDevice : public SensorDevice {

    cv::Mat *image;
    cv::Mat *depth;

    int initialize() {
        try {

        } catch (const std::exception &ex) {
            destroy();
        }

        return 0;
    }

    int capturePair() {

        return 0;
    };

    uint16_t *getDepthFrame() {

        return (uint16_t *) depth->data;
    }

    uint8_t *getColourFrame() {

        return (uint8_t *) image->data;
    }

    void destroy() {


        delete grayscale;
        delete depth;

    };

    int getDimension(Type type, Dimension dim) {
        return 0;
    }
};
