//
// Created by jaanus on 15.10.16.
//

#pragma once

#ifndef MOTION_ESTIMATOR_SENSORDEVICE_H
#define MOTION_ESTIMATOR_SENSORDEVICE_H

#include <stdint.h>
#include <string>
#include <opencv2/opencv.hpp>

class SensorDevice {

    public:
    enum Type {
        Depth, Colour
    };
    enum Dimension {Width,Height};

    protected:
    int rgbWidth = 0;
    int rgbHeight = 0;
    int depWidth = 0;
    int depHeight = 0;
    uint64_t frameTime = 0;
    bool readyness = false;
    /*uint8_t* colourFrame;
    uint16_t* depthFrame;*/



public:
    virtual int initialize() { return 0; };

    virtual uint16_t *getDepthFrame()=0;

    virtual uint8_t *getColourFrame()=0;
    virtual void destroy(){};

    int getDimension(SensorDevice::Type type, SensorDevice::Dimension dim) {
        int result = 0;
        switch (type) {
            case Depth :
                if (dim == Width) {
                    result = depWidth;
                    break;
                } else if (dim == Height) {
                    result = depHeight;
                    break;
                } else {
                    result = 0;
                    break;
                }
            case Colour :
                if (dim == Width) {
                    result = rgbWidth;
                    break;
                } else if (dim == Height) {
                    result = rgbHeight;
                    break;
                } else {
                    result = 0;
                    break;
                }
        }

        return result;
    }

    virtual int capturePair() { return 0; };

    virtual int getFrameSize(Type) { return 0; };

    virtual float getFrameTimestamp() { return 0; };

    bool isReady() {
        return readyness;
    }


};


#endif //MOTION_ESTIMATOR_SENSORDEVICE_H
