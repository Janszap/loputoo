/* 
 * File:   main.cpp
 * Author: jaanus
 *
 * Created on August 31, 2016, 8:55 PM
 */

//#define EIGEN_DONT_VECTORIZE
#define EIGEN_DONT_ALIGN_STATICALLY
//ilma selleta ei toimi debugimiseks -O0

#include <cstdlib>
#include <signal.h>
#include <OpenNI2/OpenNI.h>
#include <string>
#include <cstring>
#include <fovis/fovis.hpp>
#include <iostream>
#include <fstream>
#include <forward_list>
#include <experimental/filesystem>
#include "SensorDevice.hpp"
#include "PrimesenseDevice.hpp"
#include "RealsenseDevice.hpp"
#include "BumblebeeDevice.hpp"
#include "ImageReaderDevice.hpp"
#include "timer.h"


using namespace std;
namespace fs = std::experimental::filesystem;

sig_atomic_t shutdown = 0;
static void sig_action(int signal, siginfo_t *s, void *user) {
    cout << "Shutting down." << endl;
    shutdown = 1;
}

std::string isometryToString(const Eigen::Isometry3d& m){
    char result[128];
    memset(result, 0, sizeof(result));
    Eigen::Vector3d xyz = m.translation();
    Eigen::Vector3d rpy = m.rotation().eulerAngles(0, 1, 2);
    Eigen::Matrix4d matD = m.matrix();
    Eigen::Matrix4f matF = matD.cast<float>();
    //matF.normalize();
    //cout<<std::fixed << std::setprecision(3) <<matF(1) <<" "<< matF(2)<<" "<<matF(3)<<" "<< matF(4)<<endl;
    //Eigen::Quaternion<float> qut(matF);

    printf("%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f\n",
           xyz(0), xyz(1), xyz(2),
           rpy(0) * 180 / M_PI, rpy(1) * 180 / M_PI, rpy(2) * 180 / M_PI);

    snprintf(result, 79, "%6.2f %6.2f %6.2f %6.2f %6.2f %6.2f", 
      xyz(0), xyz(1), xyz(2), 
      rpy(0) * 180/M_PI, rpy(1) * 180/M_PI, rpy(2) * 180/M_PI);
    return std::string(result);
    
}

void writeOutputToFile(string what, string fileName) {
    ofstream file;
    file.open(fileName);
    file << what << endl;
    file.close();
}

int main(int argc, char** argv) {

    string sensorType = "";
    SensorDevice *sensor;
    fovis::CameraIntrinsicsParameters *rgb_params = new fovis::CameraIntrinsicsParameters();
    memset(rgb_params, 0, sizeof(fovis::CameraIntrinsicsParameters));
    fovis::VisualOdometryOptions defOptions = fovis::VisualOdometry::getDefaultOptions(); // an object containing the parameters for the VO algorithm
    fovis::VisualOdometry *odometry; // a pointer to the Fovis VO object
    fovis::DepthImage *depthImage; // a pointer to Fovis DepthImage object derived from the pointer to the grayscale data
    fovis::Rectification *rect; // a pointer to the object containing image rectification data

    float *depth_data; // a float array of size [numDepthPixels], is an input to Fovis odometry
    uint8_t *grayscale_data; // a pointer to the location containing grayscale image, size [numColourPixels], is an input to Fovis odometry
    uint8_t *image_data_pointer; // a temporary pointer pointing to the returned RGB image
    uint16_t *depth_data_pointer; // a temporary pointer pointing to the returned depth image
    volatile uint16_t depthValue; // a temporary depth value used in conversion loop
    uint64_t liveCounter = 0; // a debugging value tracking how many odometry loops have been completed

    /*sügavuspildi lihtsam kuvamine*/
    double min;
    double max;
    cv::Mat *adjMap;

    int numColourPixels = 0;
    int numDepthPixels = 0;

    Timer auxTimer;
    Timer loopTmr;
    Timer runWhile;
    double timer = 0.0;
    double odometryTimer = 0.0;

    cv::namedWindow("displayWindow", cv::WINDOW_AUTOSIZE);
    cv::namedWindow("depthWindow", cv::WINDOW_AUTOSIZE);

    cv::Mat *matImage;
    cv::Mat *matDepth;

    const string helpString =
            (string) "Usage: \n-s [1-4] where 1-Primesense, 2-Realsense, 3-Bumblebee 4-Image reader\n" +
                        (string) "Optional -w write to file instead of processing immediately.";

    string resultString = "";
    ostringstream *strs = new ostringstream();

    //defOptions["feature-window-size"] = "5";
    //defOptions["use-subpixel-refinement"] = "false";
    //defOptions["feature-search-window"] = "15";
    //defOptions["max-pyramid-level"] = "3";

    int opt = 0;
    int type = 0;
    bool write = false;

    while ((opt = getopt(argc, argv, "s:w")) != -1) {
        switch (opt) {
            case 's':
                type = atoi(optarg);
                break;
            case 'w':
                write = true;
                break;
            default:
                cout << helpString << endl;
                exit(EXIT_FAILURE);
        }
    }

    if (type == 1) {
        sensorType = "prime";
        sensor = new PrimesenseDevice();
    } else if (type == 2) {
        sensorType = "real";
        sensor = new RealsenseDevice();
    } else if (type == 3) {
        //TODO: bumble
        sensorType = "bumble";
        sensor = new BumblebeeDevice();
    } else if (type == 4) {
        sensorType = "imread";
        sensor = new ImageReaderDevice();
    } else {
        cout << "Wrong or missing device ID, check help (-h)." << endl;
    }

    try {
        if (sensor->initialize()) {
            //viska veateade
        }
        cout << sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width) << " "
             << sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height) << endl;

        rgb_params->width = sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width);
        rgb_params->height = sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height);
        rgb_params->cx = sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width) / 2.0;
        rgb_params->cy = sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height) / 2.0;
        if (sensorType == "prime" || sensorType == "imread") {
            rgb_params->fx = 528.49404721;
            rgb_params->fy = rgb_params->fx;
        } else if (sensorType == "real") {
            //otsi Inteli kaamera info
            rgb_params->fx = 463.889;
            rgb_params->fy = rgb_params->fx;

        } else if (sensorType == "bumble") {
            //Bumblebee 2
        }

        rect = new fovis::Rectification(*rgb_params);
        odometry = new fovis::VisualOdometry(rect, defOptions);
        depthImage = new fovis::DepthImage(*rgb_params,
                                           sensor->getDimension(SensorDevice::Type::Depth,
                                                                SensorDevice::Dimension::Width),
                                           sensor->getDimension(SensorDevice::Type::Depth,
                                                                SensorDevice::Dimension::Height));

    } catch (const std::exception &ex) {
        cout << "Big feil:\n";
        cout << ex.what() << std::endl;
        shutdown = 1;
        sensor->destroy();
    }

    numColourPixels = (sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width)) *
                      (sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height));

    numDepthPixels = (sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Width)) *
                     (sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Height));

    depth_data = new float[numDepthPixels];
    matImage = new cv::Mat(sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height),
                           sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width), CV_8UC1,
                           cv::Scalar(0));

    adjMap = new cv::Mat(sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height),
                         sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width), CV_8UC1,
                         cv::Scalar(0));

    /*create folders*/
    if (write) {
        if (!fs::exists("./rgb/")) {
            fs::create_directory("./rgb/");
        }
        if (!fs::exists("./depth/")) {
            fs::create_directory("./depth/");
        }
    }

    runWhile.reset();
    while (runWhile.elapsed() <= 30 && shutdown <= 0) {
        auxTimer.reset();
        loopTmr.reset();

        // capture a pair of depth and rgb images from the sensor
        if (sensor->capturePair()) {
            cout << "Capture failed!\n";
            break;
        }

        // get the pointers to the captured images' data locations
        image_data_pointer = sensor->getColourFrame();
        depth_data_pointer = sensor->getDepthFrame();

        // if instructed to write to file
        if (write) {
            //TODO: loo funktsioon
            *strs << sensor->getFrameTimestamp();
            cout << strs->str() << endl;
            cv::Mat tempImage = cv::Mat(
                    sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Height),
                    sensor->getDimension(SensorDevice::Type::Colour, SensorDevice::Dimension::Width),
                    CV_8UC3, (void *) image_data_pointer);
            cv::cvtColor(tempImage, tempImage, CV_BGR2RGB);
            cv::imwrite("./rgb/" + strs->str() + ".png", tempImage);
            cv::Mat tempDepth = cv::Mat(
                    sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Height),
                    sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Width),
                    CV_16UC1, (void *) depth_data_pointer);
            cv::imwrite("./depth/" + strs->str() + ".png", tempDepth);
            strs->str("");
            strs->clear();
            cout << "Written" << endl;
            continue;
        }

        // convert RGB to Grayscale with no dependence on colour order
        for (int i = 0; i < (numColourPixels * 3); i += 3) {
            matImage->data[i / 3] = (char) round((image_data_pointer[i] + image_data_pointer[i + 1] +
                                                  image_data_pointer[i + 2]) / 3);
        }
        // blur out the noise
        GaussianBlur(*matImage, *matImage, cv::Size_<int>(0, 0), 0.85);

        // create the depth image used to and blur the depth image reducing the noise
        matDepth = new cv::Mat(sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Height),
                               sensor->getDimension(SensorDevice::Type::Depth, SensorDevice::Dimension::Width),
                               CV_16UC1,
                               (void *) depth_data_pointer);
        cv::GaussianBlur(*matDepth, *matDepth, cv::Size_<int>(0, 0), 0.85);

        // convert the depth data to meters
        uint16_t *dataPointer = (uint16_t *) matDepth->data;
        for (int i = 0; i < numDepthPixels; i++) {
            depthValue = dataPointer[i];
            if (depthValue != 0) {
                depth_data[i] = (float) (depthValue / 1000);
            } else {
                depth_data[i] = std::numeric_limits<float>::quiet_NaN();
            }
        }

        // this helps to display the depth image better
        cv::minMaxIdx(*matDepth, &min, &max);
        cv::convertScaleAbs(*matDepth, *adjMap, 255 / max);

        // display the resulting images
        cv::imshow("displayWindow", *matImage);
        cv::imshow("depthWindow", *adjMap);
        cv::waitKey(1);

        // set and/or create the uint8_t grayscale and float depth data objects for Fovis VO
        grayscale_data = matImage->data;
        depthImage->setDepthImage((const float *) depth_data);
        auxTimer.reset();
        //cout<<"Odometry time."<<endl;
        odometry->processFrame(grayscale_data, depthImage);

        // if odometry fails, fail as fast as possible
        if (odometry->getMotionEstimateStatus() != 1) {
            timer = auxTimer.elapsed();
            cout << "Odometry failed in: " << timer << endl;
            continue;
        }

        // if odometry was successful, get the results
        cout << "Odometry status: " << odometry->getMotionEstimateStatus() << endl;
        odometryTimer = auxTimer.elapsed();
        cout << "Odmetry: " << timer << endl;
        auxTimer.reset();
        //        asend
        Eigen::Isometry3d pose = odometry->getPose();
        //        liikumine
        Eigen::Isometry3d motion = odometry->getMotionEstimate();
        timer = auxTimer.elapsed();
        //cout << "Gets: "<<timer << endl;
        auxTimer.reset();

        // add the current results to the output string
        *strs << liveCounter;
        *strs << "\tPose:\t" << isometryToString(pose) << endl;
        *strs << "\tMotion:\t\t" << isometryToString(motion) << endl;


        liveCounter++;
        *strs << "\tOdometry:\t" << odometryTimer << "\n\tLoop:\t\t" << loopTmr.elapsed() << endl << endl;
        delete matDepth;
    }

    writeOutputToFile(strs->str(), "Output.txt");
    sensor->destroy();

    delete sensor;
    delete[] depth_data;
    delete matImage;
    delete adjMap;
    delete depthImage;
    delete rgb_params;
    delete rect;
    delete odometry;
    delete strs;
    //delete depth_data_pointer;

    return 0;
}