//
// Created by jaanus on 16.10.16.
//

#pragma once

#include "SensorDevice.hpp"
#include <stdint.h>
#include <librealsense/rs.hpp>
#include <string>

class BumblebeeDevice : public SensorDevice {

    rs::device *rDev;

    int initialize() {
        try {

        } catch (const rs::error &e) {
            destroy();
        }


        return 0;
    };

    virtual int capturePair() { return 0; };

    uint16_t *getDepthFrame() {

        return 0;
    }

    uint8_t *getColourFrame() {

        return 0;
    }

    void destroy() {
        rDev->stop();
    };

    int getDimension(Type type, Dimension dim) {
        return 0;
    }
};
