//
// Created by jaanus on 11.02.17.
// Source: http://stackoverflow.com/questions/728068/how-to-calculate-a-time-difference-in-c
//

#ifndef MOTION_ESTIMATOR_TIMER_H
#define MOTION_ESTIMATOR_TIMER_H

#include <iostream>
#include <chrono>

class Timer {
public:
    Timer() : beg_(clock_::now()) {}

    void reset() { beg_ = clock_::now(); }

    double elapsed() const {
        return std::chrono::duration_cast<second_>
                (clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};

#endif //MOTION_ESTIMATOR_TIMER_H
