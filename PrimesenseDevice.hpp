//
// Created by jaanus on 15.10.16.
//

#pragma once

#include "SensorDevice.hpp"
#include <stdint.h>
#include <OpenNI2/OpenNI.h>
#include <iostream>
#include <string>
#include <OpenNI.h>

openni::Device pDev;
openni::VideoStream depthStream, colourStream;
openni::VideoStream *streams[2];
openni::VideoFrameRef *rgbFrame, *depthFrame;
const char* deviceURI = openni::ANY_DEVICE;
openni::Status sitrep;

using namespace std;

class PrimesenseDevice : public SensorDevice  {


    int initialize() {
        sitrep = openni::STATUS_OK;

        sitrep = openni::OpenNI::initialize();
        //cout << "Sitrep: " << sitrep << endl;
        sitrep = pDev.open(deviceURI);
        //cout << "Sitrep: " << sitrep << endl;
        sitrep = depthStream.create(pDev, openni::SENSOR_DEPTH);
        //cout << "Sitrep: " << sitrep << endl;
        sitrep = colourStream.create(pDev, openni::SENSOR_COLOR);
        //cout << "Sitrep: " << sitrep << endl;

        //sitrep = pDev.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
        //cout << "Sitrep: " << sitrep << endl;
        //sitrep = pDev.setDepthColorSyncEnabled(true);
        //cout << "Sitrep: " << sitrep << endl;

        sitrep = depthStream.start();
        //cout << "Sitrep: " << sitrep << endl;
        sitrep = colourStream.start();
        //cout << "Sitrep: " << sitrep << endl;

        //pDev.setDepthColorSyncEnabled(true);
        //pDev.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

        streams[0] = &depthStream;
        streams[1] = &colourStream;

        rgbFrame = new openni::VideoFrameRef();
        depthFrame = new openni::VideoFrameRef();

        rgbWidth = colourStream.getVideoMode().getResolutionX();
        rgbHeight = colourStream.getVideoMode().getResolutionY();
        depWidth = depthStream.getVideoMode().getResolutionX();
        depHeight = depthStream.getVideoMode().getResolutionY();

        readyness = true;

        return 0;
    }

    int getFrameSize (SensorDevice::Type type) {
        switch (type) {
            case Depth:
                return depthFrame->getDataSize();
            case Colour:
                return rgbFrame->getDataSize();
            default:
                return 0;
        }
    }

    int capturePair() {
        int changedStreamDummy = 0;
        //sitrep = openni::OpenNI::waitForAnyStream(streams, 1, &changedStreamDummy, openni::TIMEOUT_FOREVER);
        //cout << "Sitrep, wait: " << sitrep << endl;

        if (depthStream.isValid() && colourStream.isValid()) {
            sitrep = depthStream.readFrame(depthFrame);
            //cout << "Sitrep, depth: " << sitrep << endl;
            sitrep = colourStream.readFrame(rgbFrame);
            //cout << "Sitrep, colour: " << sitrep << endl;
        } else {
            return 1;
        }
        return 0;
    }

    uint16_t *getDepthFrame() {
        return (uint16_t *) depthFrame->getData();
    }

    uint8_t *getColourFrame() {
        return (uint8_t *) rgbFrame->getData();
    }

    float getFrameTimestamp() {
        float rgb = rgbFrame->getTimestamp();
        float depth = rgbFrame->getTimestamp();
        return ((rgb + depth) / 2);
    };

    void destroy() {
        //delete data;
        //delete conversion;
        rgbFrame->release();
        depthFrame->release();
        depthStream.stop();
        colourStream.stop();
        depthStream.destroy();
        colourStream.destroy();
        pDev.close();
        openni::OpenNI::shutdown();
    }
};
