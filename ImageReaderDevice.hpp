//
// Created by jaanus on 16.10.16.
//

#pragma once

#include "SensorDevice.hpp"
#include <stdint.h>
#include <string>
#include <opencv2/opencv.hpp>
#include <experimental/filesystem>

using namespace std;
using namespace std::experimental::filesystem;

class ImageReaderDevice : public SensorDevice {

private:
    cv::Mat *image;
    cv::Mat *depth;
    string imageRoot = "./";
    vector<std::string> rgbFilenames;
    vector<std::string> depthFilenames;
    string currentFilename = "";
    const directory_iterator end{};



    int initialize() {
        try {
            //rgb failide nimistu ja sorteerimine
            for (directory_iterator iter{imageRoot + "rgb/"}; iter != end; ++iter) {
                rgbFilenames.push_back(iter->path().string());
            }
            std::sort(rgbFilenames.begin(), rgbFilenames.end());
            //depth failinimistu ja sorteerimine
            for (directory_iterator iter{imageRoot + "depth/"}; iter != end; ++iter) {
                depthFilenames.push_back(iter->path().string());
            }
            std::sort(depthFilenames.begin(), depthFilenames.end());

            cv::Mat tempImage = cv::imread(rgbFilenames.begin().base()->data(), cv::IMREAD_UNCHANGED);
            rgbWidth = tempImage.cols;
            rgbHeight = tempImage.rows;
            cv::Mat tempDepth = cv::imread(depthFilenames.begin().base()->data(), cv::IMREAD_UNCHANGED);
            depWidth = tempDepth.cols;
            depHeight = tempDepth.rows;

            /*cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
            cv::imshow( "Display window", tempDepth );
            cv::waitKey(10000);*/
            delete &tempImage, &tempDepth;
            image = new cv::Mat(rgbHeight, rgbWidth, CV_8UC1);
            depth = new cv::Mat(depHeight, depWidth, CV_16UC1);

        } catch (const std::exception &ex) {
            destroy();
        }
        return 0;
    }

    int capturePair() {
        *image = cv::imread(rgbFilenames.begin().base()->data(), cv::IMREAD_UNCHANGED);
        rgbFilenames.erase(rgbFilenames.begin());
        *depth = cv::imread(depthFilenames.begin().base()->data(), cv::IMREAD_UNCHANGED);
        depthFilenames.erase(depthFilenames.begin());
        return 0;
    };

    uint16_t *getDepthFrame() {
        return (uint16_t *) depth->data;
    }

    uint8_t *getColourFrame() {
        cv::cvtColor(*image, *image, CV_BGR2RGB);
        return (uint8_t *) image->data;
    }

    float getFrameTimestamp() {
        long temp = std::stof(currentFilename.substr(0, currentFilename.find_last_of('.')));
        return 0;
    };

    void destroy() {
        delete image;
        delete depth;

    };

};
