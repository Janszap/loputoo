//
// Created by jaanus on 16.10.16.
//

#pragma once

#include "SensorDevice.hpp"
#include <stdint.h>
#include <librealsense/rs.hpp>
#include <string>
#include <opencv2/opencv.hpp>

class RealsenseDevice : public SensorDevice {

    rs::device* rDev;
    rs::context cnt;
    bool depth = true;
    bool colour = true;
    float timestamp = 0.0;
    const cv::Mat *tempImage;
    const cv::Mat *resizedImage;
    uint8_t *colour_frame;
    uint16_t *depth_frame;

    int initialize() {
        try {
            //rs::context cnt;
            if (cnt.get_device_count() == 0) return 1;
            rDev = cnt.get_device(0);

            try {
                rDev->enable_stream(rs::stream::depth, 628, 468, rs::format::z16, 30);
                rDev->enable_stream(rs::stream::color, 640, 480, rs::format::rgb8, 30);
            } catch (rs::error &error) {
                cout << error.what();
            }

            rDev->start();

            //vajalik warmup
            for (int i = 0; i < 30; i++)
                rDev->wait_for_frames();

            tempImage = new cv::Mat(rDev->get_stream_height(rs::stream::depth),
                                    rDev->get_stream_width(rs::stream::depth), CV_16UC1);
            resizedImage = new cv::Mat(640, 480, CV_16UC1);

            rgbWidth = 640;
            rgbHeight = 480;
            depWidth = 640;
            depHeight = 480;

            readyness = true;
        } catch (const rs::error & e){
            e.what();
            destroy();
        }
        return 0;
    };

    int capturePair() {
        try {
            rDev->wait_for_frames();
            colour_frame = (uint8_t *) (rDev->get_frame_data(rs::stream::color));
            depth_frame = (uint16_t *) (rDev->get_frame_data(rs::stream::depth));
            timestamp = rDev->get_frame_timestamp(rs::stream::color);
            delete (tempImage);
            tempImage = new cv::Mat(rDev->get_stream_width(rs::stream::depth),
                                    rDev->get_stream_height(rs::stream::depth), CV_16UC1, (void *) depth_frame);
            cv::resize(*tempImage, *resizedImage, resizedImage->size(), 0, 0, cv::INTER_LINEAR);
            depth_frame = (uint16_t *) resizedImage->data;

        } catch (const rs::error &e) {
            return 1;
        }
        return 0;
    }

    uint16_t *getDepthFrame() {
        return depth_frame;
    }

    uint8_t *getColourFrame() {
        return colour_frame;
    }

    float getFrameTimestamp() {
        return timestamp;
    }

    void destroy() {
        rDev->stop();
        rDev->disable_stream(rs::stream::color);
        rDev->disable_stream(rs::stream::depth);
    };

};
